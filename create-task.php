<?php
@ob_start();
session_start();
?>

<!DOCTYPE html>
<html>
<head>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    

    <style>
      
      button{
        float: right;
      }
        .data{
          display: none;
        }

        .top-buttons{
          list-style-type: none;
          display: inline-flex;
        }
        h2{
      padding-top: 0.5rem;
       margin-bottom: 0px; 
    }
    #mySelect{
      
      width: 15%;
      
    }
    </style>


</head>

<main>





  <div class="container">


    <div class="row welcome text-center mb-4 mt-4" style="border-bottom: 1px solid gray">


      <div class="col  md-4 ">
        <h2>Create product list</h2>
      </div>

      <div class="col  md-8">

    <form action="includes/validate-task.php" method="POST" >
        <ul class="top-buttons p-1 m-3">
          <li class="pr-3">
            <a href="index.php" class="btn btn-info" role="button">Cancel</a>
          </li>
          <li>

              <button type="submit" class="btn btn-success" name="submit">Add</button>
          </li>
        </ul>
      </div>

    </div>





        <div class="form-group" style="width: 50%">
            <label>SKU</label>
            <input type="text" class="form-control" name="sku" placeholder="SKU" required></input>
            <label>Name</label>
            <input type="text" class="form-control" name="name" placeholder="Name" required></input>
            <label>Price ($)</label>
            <input type="text" class="form-control" name="price" placeholder="Price" required></input>
        </div>



        <div class="menu">
        <label>Type selector</label>
          <select id="mySelect" name="selector">
            <option hidden select value>Select Type</option>
            <option value="dvd">DVD</option>
            <option value="book">Book</option>
            <option value="furniture">Furniture</option>
          </select>
        </div>

      <div class="content pt-3 pb-3" style="width: 50%;">
        <div class="data" id="dvd">
          <label>Size (MB)</label>
          <input type="text" class="form-control" name="size" placeholder="Size MB" ></input>
          <p>Please provide DVD size in Megabytes</p>
        </div>
        <div class="data" id="book">
          <label>Book (KG)</label>
          <input type="text" class="form-control" name="weight" placeholder="Weight KG"></input>
          <p>Please provide book weight in Kilograms</p>

        </div>
        <div class="data" id="furniture">
          <label>Height CM</label>
          <input type="text" class="form-control" name="height" placeholder="Height"></input>
          <label>Width CM</label>
          <input type="text" class="form-control" name="width" placeholder="Width"></input>
          <label>Length CM</label>
          <input type="text" class="form-control" name="length" placeholder="Length"></input>
          <p>Please provide dimensions in HxWxL</p>
        </div>
      </div>
      
      </form>
      <div class="errorMessages">
      <?php
                //display errors
                $fullUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                //display errors
                if(strpos($fullUrl, "product=error") == true){
                     
                    $errorMessages = $_SESSION['errorMessages'];
                    foreach ($errorMessages as $errorMessage) {
                        echo "<p>$errorMessage</p>";
                    }
                    
            }
          
                 
              
              ?>
          </div>
    </div>
</main>



<script>
    //dropdown animation
    $(document).ready(function(){
      $("#mySelect").on('change', function(){


          $(".data").hide();
        $("#" + $(this).val()).fadeIn(300);


      }).change();
    });

</script>
</html>
