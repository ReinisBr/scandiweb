<?php
//database connection
$dbServername = "localhost";
$dbUsername = "root";
$dbPassword = "";
$dbName = "product-project";


$conn = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);

if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}
