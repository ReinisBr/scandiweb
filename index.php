<?php
session_start();
 include 'includes/dbh.inc.php';
//selects from
$tasks = mysqli_query($conn, "SELECT * FROM tasks");


 ?>
<!DOCTYPE html>
<html>
  <head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <style>

   
    .checkbox{
      transform: scale(1.5);
      margin-bottom: 1rem;

    }
    .top-buttons{
      list-style-type: none;
      display: inline-flex;
    }
    .check{
      padding: 0.5rem 0.5rem;
      margin: 0;
      padding-bottom: 0px;
    }
    .card-body{
      padding-top: 0,rem;
    }

    h1{
      padding-top: 0.5rem;
       margin-bottom: 0px; 
    }

  </style>
<title>Products</title>

  </head>
  <body>


    <div class="container">
      <div class="row welcome text-center mb-4 mt-4" style="border-bottom: 1px solid gray">


        <div class="col  md-4 ">
          <h1>Product list</h1>
        </div>

        <div class="col  md-8">
<form method="post">
          <ul class="top-buttons p-1 m-3">
            <li class="pr-3">
              <a href="create-task.php" class="btn btn-primary mb-4 piev" role="button">Add</a>
            </li>
            <li>

                <input  class="btn btn-danger"type="submit" name="delete"  value="Mass delete">
            </li>
          </ul>
        </div>

      </div>






      <?php while ($row = mysqli_fetch_array($tasks)){ ?>
        <div class="card mr-3 mb-3" style="width: 12rem; display: inline-flex;">
    <p class="check"><input type="checkbox" name="checkbox[]" value="<?php echo $row['id']; ?>"/></p>
    <div class="card-body text-center mt-0">
      <h5 class="card-title mt-0"><?php echo $row['sku']; ?></h5>
      <h5 class="card-title "><?php  echo  $row['name']; ?></h5>
      <h5 class="card-title"><?php  echo  $row['price']; ?>$</h5>
      <?php 
      //displays data depending of type
      if($row['parameters_type'] == "dvd"){ 
				 	echo "<h5>Size : " . $row['Parameters'] . "MB</h5>"; 
				}else if($row['parameters_type'] == "book"){ 
					echo "<h5>Weight : " . $row['Parameters'] . "KG</h5>";
				}else if($row['parameters_type'] == "furniture"){
					echo "<h5>Dimensions : " . $row['Parameters'] . "</h5>";
				}?>
      



    </div>
  </div>

    
    
    
    
    
    <?php   } ?>
    
</form>
      </div>

     
        <?php
        if (isset($_POST['delete'])) {
          $check = $_POST['checkbox'];
          foreach ($check as $id) {
             mysqli_query($conn, "DELETE FROM tasks WHERE id =$id;");
          }
          
        }
        mysqli_close($conn);

       ?>




  </body>
</html>
